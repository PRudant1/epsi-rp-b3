Gitlab-CI : TP Noté
Rendu attendu au choix :
- Le fichier .gitlab-ci.yml
- Le lien de votre repository

Vous devez réaliser différent déclenchement :
- Déclenchement sur une Merge Request
- Déclenchement sur une branch
- Déclenchement planifier (schedule)
- Déclenchement sur la modification d'un fichier
- Déclenchement manuel avec variable

Bonus : (Point en plus)
- Utilisation d’image différente depuis le pipeline (1 point)
- Utilisation d’artefact (4 points)

- Vous devez alimenter un fichier entre tout les job
Exemple :
- Job 1 : le fichier contient “Je suis passé dans le Job 1”
- Job 2 : le fichier contient “Je suis passé dans le Job 1 et le Job 2”
- Job 3 : le fichier contient “Je suis passé dans le Job 1 et le Job 2 et leJob 3”....
